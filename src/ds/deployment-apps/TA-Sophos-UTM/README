This TA has been build for CIM compliance logs for the Sophos UTM 9 product line. 

This app utilizes index-time extractions for metadata populating and index routing. 

Environment Configuration:
- Set all UTM 9 devices to forward logs to a syslog server
- Install UF on Syslog Server
- Either manually or via Deployment Server place an input on the Syslog Server's UF 
  to grab the Sophos logs. Set the sourcetype to "sophos"
- Place the TA on the indexers and search heads for field extractions

TA Configuration:
- The Sophos TA uses TRANSFORMS to convert the sophos:utm sourcetype to firewall events, 
  http proxy events, and intrusion detection events based on these sourcetypes:
  	- sophos:utm:fw
	- sophos:utm:httpproxy
	- sophos:utm:ids
- Fieldaliases are used for CIM compliance to the following Data Models:
	- Network Traffic
	- Intrusion Detection
	- Web
- Transforms grab the accurate host name as the second host name in the log. Remove this 
  if the syslog server is updated with accurate domain resolution
- Transforms can place the events in to their corresponding indexes
	- Firewall - netfw
	- Http Proxy - netproxy
	- IDS - netids
- Add the following line to local/props.conf and adjust individual transforms in local/transforms.conf to suite local index naming standards
TRANSFORMS-0force_sophos_utm_index = sophos_utm_ips_index, sophos_utm_fw_index, sophos_utm_http_index

