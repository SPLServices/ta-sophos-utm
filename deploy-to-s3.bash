#!/bin/bash
for target in *.spl
do
	aws s3 cp $target s3://seckitbuilds/Release/`echo "$target" | sed 's/\.tar\.gz$/\.spl/'`
done
